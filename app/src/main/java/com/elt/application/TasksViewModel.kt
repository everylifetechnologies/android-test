package com.elt.application

import android.content.Context

class TasksViewModel(tasksApiService: TasksApiServicing) : TasksViewModelInterface {
    var tasksApiService: TasksApiServicing? = null
    var tasks: Array<Task>? = null
    var filteredTasks: Array<Task>? = null

    init {
        this.tasksApiService = tasksApiService
    }

    override fun reloadTable(context: Context) {
        tasks = tasksApiService?.getTasks(context)
        filteredTasks = tasks
    }

    override fun beginRefreshing() {
        TODO("A: implement this so that the progressBar is shown only when loading. " +
                    "The progressBar should also be adjusted to be central at the top of the screen"
        )
    }

    override fun endRefreshing() {
        TODO("A: implement this so that the progressBar is shown only when loading. ")
    }

    override fun getFilteredTasksList(): Array<Task>? {
        return filteredTasks
    }

    override fun filterClicked(tag: Any?) {
        TODO("B: implement this so that the list will show only the tasks that have the selected TaskType")
    }
}