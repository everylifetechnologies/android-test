package com.elt.application

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class TaskViewHolder(taskView: View) : RecyclerView.ViewHolder(taskView) {
    val nameLabel: TextView
    val descriptionLabel: TextView
    val typeIconImageView: ImageView

    init {
        nameLabel = taskView.findViewById(R.id.name)
        descriptionLabel = taskView.findViewById(R.id.description)
        typeIconImageView = taskView.findViewById(R.id.task_type)
    }
}