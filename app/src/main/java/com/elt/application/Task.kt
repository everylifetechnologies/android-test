package com.elt.application

data class Task(var id: Int, var name: String, var description: String, var type: TaskType)