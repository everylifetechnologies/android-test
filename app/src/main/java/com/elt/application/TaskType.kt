package com.elt.application

enum class TaskType {
     general,
     medication,
     hydration,
     nutrition
}