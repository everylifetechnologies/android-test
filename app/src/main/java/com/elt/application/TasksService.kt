package com.elt.application

interface TasksServiceInterface {
    fun getTasks(): Array<Task>
}

class TasksService:TasksServiceInterface {
    override fun getTasks(): Array<Task> {
        TODO("A: Implement this with the help of TasksApiService")
    }
}