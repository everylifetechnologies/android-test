package com.elt.application

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class TasksListFragment : Fragment() {

    private lateinit var viewModel: TasksViewModelInterface

    enum class LayoutManagerType { GRID_LAYOUT_MANAGER, LINEAR_LAYOUT_MANAGER }

    private lateinit var currentLayoutManagerType: LayoutManagerType
    private lateinit var recyclerView: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = TasksViewModel(TasksApiService())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.tasks_list_fragment, container, false).apply { tag = TAG }

        recyclerView = rootView.findViewById(R.id.recyclerView)
        layoutManager =
            LinearLayoutManager(activity)
        currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER

        if (savedInstanceState != null) {
            currentLayoutManagerType = savedInstanceState.getSerializable(KEY_LAYOUT_MANAGER) as LayoutManagerType
        }
        setRecyclerViewLayoutManager(currentLayoutManagerType)

        context?.let {
            viewModel.reloadTable(it)
            viewModel.getFilteredTasksList()?.let {
                recyclerView.adapter = TasksListAdapter(it)
            }
        }

        val filterGeneral: View = rootView.findViewById(R.id.filter_general)
        filterGeneral.setOnClickListener {
            viewModel.filterClicked(TaskType.general)
        }
        val filterMedication: View = rootView.findViewById(R.id.filter_medication)
        filterMedication.setOnClickListener {
            viewModel.filterClicked(TaskType.medication)
        }
        val filterHydration: View = rootView.findViewById(R.id.filter_hydration)
        filterHydration.setOnClickListener {
            viewModel.filterClicked(TaskType.hydration)
        }
        val filterNutrition: View = rootView.findViewById(R.id.filter_nutrition)
        filterNutrition.setOnClickListener {
            viewModel.filterClicked(TaskType.nutrition)
        }
        return rootView
    }

    private fun setRecyclerViewLayoutManager(layoutManagerType: LayoutManagerType) {
        var scrollPosition = 0

        if (recyclerView.layoutManager != null) {
            scrollPosition = (recyclerView.layoutManager as LinearLayoutManager)
                .findFirstCompletelyVisibleItemPosition()
        }

        when (layoutManagerType) {
            LayoutManagerType.GRID_LAYOUT_MANAGER -> {
                layoutManager = GridLayoutManager(
                    activity,
                    SPAN_COUNT
                )
                currentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER
            }
            LayoutManagerType.LINEAR_LAYOUT_MANAGER -> {
                layoutManager =
                    LinearLayoutManager(activity)
                currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER
            }
        }

        with(recyclerView) {
            layoutManager = this@TasksListFragment.layoutManager
            scrollToPosition(scrollPosition)
        }
    }


    companion object {
        private val TAG = "TasksListFragment"
        private val KEY_LAYOUT_MANAGER = "layoutManager"
        private val SPAN_COUNT = 2
    }
}