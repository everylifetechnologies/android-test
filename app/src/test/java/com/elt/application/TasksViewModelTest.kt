package com.elt.application

import android.content.Context
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class TasksViewModelTest {
    @Test
    fun testRefreshTasksWithOnlyOneTaskWillOnlyReturnOneFilteredTask() {
        // Given
        val task = Task(1, "some name", "some description",  TaskType.general)
        val tasksService = MockTasksService(arrayOf(task))
        val viewModel = TasksViewModel(tasksService)

        // When
        viewModel.reloadTable(RuntimeEnvironment.getApplication())

        // Then
        assertThat(1, equalTo(viewModel.getFilteredTasksList()?.size))
        assertThat(1, equalTo(viewModel.getFilteredTasksList()?.get(0)?.id))
        assertThat("some name", equalTo(viewModel.getFilteredTasksList()?.get(0)?.name))
        assertThat("some description", equalTo(viewModel.getFilteredTasksList()?.get(0)?.description))
    }

    fun testFilterTasksByGeneralWillOnlyReturnGeneralTasks() {
        // TODO B: Implement this test
    }

    // TODO B: Add any other relevant tests

    private class MockTasksService(tasks: Array<Task>) : TasksApiServicing {

        private var tasks: Array<Task>

        init {
            this.tasks = tasks
        }

        override fun getTasks(context: Context): Array<Task> {
            return tasks
        }
    }
}
